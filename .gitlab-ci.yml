variables:
  TXT_E: "\e[31m"
  TXT_S: "\e[35m"
  TXT_W: "\e[33m"
  TXT_CLEAR: "\e[0m"

stages:
  - check
  - build
  - deploy

run_pre_commit:
  stage: check
  tags:
    - ubuntu20.04
  script:
    - scripts/pre_commit_test.sh

.build_with_conan: &build_with_conan
  stage: build
  variables:
    ESYSTEST_CONAN_USER_CHANNEL: "libesys/testing"
  script:
    - scripts/conan_configure.sh
    - scripts/conan_build_pkg.sh $ESYSTEST_CONAN_USER_CHANNEL
  rules:
    - if: "$CI_COMMIT_MESSAGE =~ /^bump:/"
      when: never
    - if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH"
      when: never
    - if: '$CI_COMMIT_MESSAGE =~ /\[ci_test_master]/'
      when: never
    - if: $CI_COMMIT_TAG
      when: never
    - if: '$CI_PIPELINE_SOURCE == "push"'

.create_conan_pkg_base: &create_conan_pkg_base
  stage: build
  script:
    - scripts/conan_configure.sh
    - scripts/conan_build_pkg.sh $ESYSTEST_CONAN_USER_CHANNEL
    # - scripts/conan_publish_gitlab.sh $ESYSTEST_CONAN_AT_USER_CHANNEL
    - scripts/conan_publish_libesys.sh $LIBESYS_CONAN_USER $LIBESYS_CONAN_PASSWORD $ESYSTEST_CONAN_AT_USER_CHANNEL
    # - scripts/conan_publish_jfrogio.sh $LIBESYS_CONAN_USER $LIBESYS_CONAN_PASSWORD $ESYSTEST_CONAN_AT_USER_CHANNEL

.create_conan_pkg_testing: &create_conan_pkg_testing
  <<: *create_conan_pkg_base
  variables:
    ESYSTEST_CONAN_USER_CHANNEL: "libesys/testing"
    ESYSTEST_CONAN_AT_USER_CHANNEL: "@$ESYSTEST_CONAN_USER_CHANNEL"
  rules:
    - if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH"
      when: never
    - if: '$CI_COMMIT_MESSAGE =~ /\[ci_test_master]/'
      when: never
    - if: "$CI_COMMIT_MESSAGE =~ /^bump:/"

.create_conan_pkg: &create_conan_pkg
  <<: *create_conan_pkg_base
  variables:
    ESYSTEST_CONAN_USER_CHANNEL: ""
    ESYSTEST_CONAN_AT_USER_CHANNEL: ""
  rules:
    - if: "$CI_COMMIT_MESSAGE !~ /^bump:/"
      when: never
    - if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH"
    - if: '$CI_COMMIT_MESSAGE =~ /\[ci_test_master]/'

build_with_conan:ubuntu20_04:
  <<: *build_with_conan
  tags:
    - ubuntu20.04

build_with_conan:ubuntu22_04:
  <<: *build_with_conan
  tags:
    - ubuntu22.04

build_with_conan:macos_big_sur:
  <<: *build_with_conan
  tags:
    - macos

create_conan_pkg_testing:ubuntu20_04:
  <<: *create_conan_pkg_testing
  tags:
    - ubuntu20.04

create_conan_pkg:ubuntu20_04:
  <<: *create_conan_pkg
  tags:
    - ubuntu20.04

create_conan_pkg_testing:ubuntu22_04:
  <<: *create_conan_pkg_testing
  tags:
    - ubuntu22.04

create_conan_pkg:ubuntu22_04:
  <<: *create_conan_pkg
  tags:
    - ubuntu22.04

create_conan_pkg_testing:macos_big_sur:
  <<: *create_conan_pkg_testing
  tags:
    - macos

create_conan_pkg:macos_big_sur:
  <<: *create_conan_pkg
  tags:
    - macos

.unit_test: &unit_test
  stage: build
  script:
    - if [ -z "${SKIP_SONAR_CLOUD}" ]; then scripts/install_sonarcloud.sh; fi
    - scripts/bootstrap.sh
    - scripts/build_ci_merge.sh
    - if [ -z "${SKIP_CODE_COVERAGE}" ]; then scripts/run_code_coverage.sh; else scripts/run_unittests.sh; fi
    - if [ -z "${SKIP_SONAR_CLOUD}" ]; then scripts/run_sonar_scanner.sh; fi
    - if [ -z "${SKIP_VALGRIND}" ]; then scripts/valgrind_unittests.sh; fi
    - if [ -z "${SKIP_CLANG_TIDY}" ]; then scripts/run_clang_tidy.sh; fi
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /WIP$/
      when: never
  artifacts:
    paths:
      - public
    reports:
      junit:
        - build_dev/*_junit.txt
        - public/logs/*_junit.txt

unit_tests:ubuntu20_04:
  <<: *unit_test
  tags:
    - ubuntu20.04
  rules:
    - if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH"
      when: never
    - if: '$CI_PIPELINE_SOURCE == "push"'

unit_tests:ubuntu22_04:
  <<: *unit_test
  tags:
    - ubuntu22.04
  rules:
    - if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH"
      when: never
    - if: '$CI_PIPELINE_SOURCE == "push"'

unit_tests:macos_big_sur:
  <<: *unit_test
  variables:
    SKIP_SONAR_CLOUD: "yes"
    SKIP_CODE_COVERAGE: "yes"
    SKIP_VALGRIND: "yes"
    SKIP_CLANG_TIDY: "yes"
    SKIP_DOC: "yes"
  tags:
    - macos
  rules:
    - if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH"
      when: never
    - if: '$CI_PIPELINE_SOURCE == "push"'

.webpage: &webpage
  stage: build
  tags:
    - ubuntu20.04
  script:
    - scripts/bootstrap.sh
    - scripts/build_webpage.sh
  artifacts:
    paths:
      - public

test_webpage:
  <<: *webpage
  except:
    - master

webpage:
  <<: *webpage
  only:
    - master

pages:
  <<: *unit_test
  tags:
    - ubuntu20.04
  stage: deploy
  needs: ["webpage"]
  rules:
    - if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH"

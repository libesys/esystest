/*!
 * \file esystest_t/unittest.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#define ESYSTEST_TEST_MAIN

#include <esystest/unit_test.h>
#include <esystest/testcasectrl.h>
#include <esystest/stdlogger.h>

#include <esystest/vld.h>

#include <iostream>

class GlobalInit : public esystest::TestCaseCtrl
{
public:
    GlobalInit();
    ~GlobalInit();

protected:
    esystest::StdLogger m_logger;
    esystest::TestCaseCtrl m_test_ctrl;
};

GlobalInit::GlobalInit()
    : esystest::TestCaseCtrl()
{
    m_logger.Set(&std::cout);
    esystest::Logger::Set(&m_logger);
}

GlobalInit::~GlobalInit()
{
}

ESYSTEST_GLOBAL_FIXTURE(GlobalInit);

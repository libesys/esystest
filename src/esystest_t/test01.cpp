/*!
 * \file esystest_t/test01.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esystest_t/esystest_t_prec.h"
// #include <boost/test/unit_test.hpp>
#include <esystest/inttypes.h>

BOOST_AUTO_TEST_CASE(Test01)
{
    int32_t result;

    result = 0;
    BOOST_REQUIRE_EQUAL(0, result);

    BOOST_REQUIRE_NE(1, result);

    BOOST_REQUIRE_GE(1, result);

    BOOST_REQUIRE_LT(-1, result);
}

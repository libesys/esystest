/*!
 * \file esystest/multios/testcase_multios.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esystest/esystest_prec.h"
#include "esystest/multios/testcase.h"
#include "esystest/testcasectrl.h"
#include "esystest/testcaseinfo.h"

#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>

namespace esystest::multios
{

TestCase::TestCase(TestCaseInfo *info)
    : TestCaseBase(info)
{
}

TestCase::~TestCase() = default;

std::string TestCase::create_test_temp_folder(const std::string &folder, bool delete_if_exist,
                                              bool test_name_lower_case)
{
    if (get_info() == nullptr) return "";

    boost::filesystem::path temp_path = TestCaseCtrl::Get().GetTempFilesFolder();
    boost::filesystem::path folder_path = temp_path;

    std::string folder_name = get_info()->GetName();
    if (test_name_lower_case) boost::algorithm::to_lower(folder_name);

    if (!folder.empty()) folder_path /= folder;
    folder_path /= folder_name;
    if (boost::filesystem::exists(folder_path) && delete_if_exist) boost::filesystem::remove_all(folder_path);

    bool result = boost::filesystem::create_directories(folder_path);
    if (result) return folder_path.string();
    return "";
}

} // namespace esystest::multios

/*!
 * \file esystest/types.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esystest/esystest_prec.h"
#include "esystest/types.h"
#include "esystest/testcaseinfo.h"

ESYSTEST_API esystest::TestCaseInfo &operator*(esystest::TestCaseInfo &info, const esystest::nil_t &n)
{
    return info;
}

/*!
 * \file esystest/em/testcasectrlcore_em.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esystest/esystest_prec.h"
#include "esystest/em/testcasectrlcore.h"

namespace esystest
{

namespace em
{

TestCaseCtrlCore::TestCaseCtrlCore()
    : TestCaseCtrlBase()
{
}

TestCaseCtrlCore::~TestCaseCtrlCore()
{
}

void TestCaseCtrlCore::Invoke(TestCaseInfo *cur_test)
{
}

} // namespace em

} // namespace esystest

/*!
 * \file esystest/testcase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esystest/esystest_prec.h"
#include "esystest/testcase.h"

namespace esystest
{

unsigned int TestCaseBase::g_count = 0;

unsigned int TestCaseBase::GetCount()
{
    return g_count;
}

TestCaseBase::TestCaseBase(TestCaseInfo *info)
    : m_info(info)
{
    ++g_count;
}

TestCaseBase::~TestCaseBase() = default;

void TestCaseBase::set_info(TestCaseInfo *info)
{
    m_info = info;
}

const TestCaseInfo *TestCaseBase::get_info() const
{
    return m_info;
}

TestCaseInfo *TestCaseBase::get_info()
{
    return m_info;
}

} // namespace esystest

/*!
 * \file esystest/globalfixture.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esystest/esystest_prec.h"
#include "esystest/globalfixture.h"

namespace esystest
{

GlobalFixture *GlobalFixture::m_last = nullptr;

GlobalFixture::GlobalFixture()
{
    m_prev = GlobalFixture::m_last;
    GlobalFixture::m_last = this;
}

GlobalFixture::~GlobalFixture()
{
}

} // namespace esystest

/*!
 * \file esystest/order.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esystest/esystest_prec.h"
#include "esystest/esystest/order.h"
#include "esystest/testcaseinfo.h"

namespace esystest
{

namespace esystest_impl
{

Order::Order(int value)
    : m_order(value)
{
}

void Order::set_value(int order)
{
    m_order = order;
}

int Order::value() const
{
    return m_order;
}

ESYSTEST_API Order order(int value)
{
    Order order_(value);
    return order_;
}

} // namespace esystest_impl

} // namespace esystest

ESYSTEST_API esystest::TestCaseInfo &operator*(esystest::TestCaseInfo &info,
                                               const esystest::esystest_impl::Order &order)
{
    info.SetOrder(order.value());
    return info;
}

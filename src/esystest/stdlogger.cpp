/*!
 * \file esystest/stdlogger.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esystest/esystest_prec.h"
#include "esystest/stdlogger.h"

namespace esystest
{

StdLogger::StdLogger()
    : Logger_t<std::ostream>()
{
}

StdLogger::~StdLogger()
{
}

Logger &StdLogger::endl()
{
    if (m_logger != nullptr) *m_logger << std::endl;
    return *this;
}

} // namespace esystest

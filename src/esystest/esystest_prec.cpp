/*!
 * \file esystest/esystest_prec.cpp
 * \brief For precompiled headers
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

// esystest.cpp : source file that includes just the standard includes
// esystest.pch will be the pre-compiled header
// esystest.obj will contain the pre-compiled type information

#include "esystest/esystest_prec.h"

// TODO: reference any additional headers you need in esystest_prec.h
// and not in this file

/*!
 * \file esystest/boost/testcasectrl.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esystest/esystest_prec.h"
#include "esystest/boost/boost/testcasectrl.h"
#include "esystest/testcaseinfo.h"
#include "esystest/mastertestsuite.h"
#include "esystest/exception.h"
#include "esystest/assert.h"

#include <string.h>
#include <iostream>

#include <boost/program_options/cmdline.hpp>
#include <boost/program_options/parsers.hpp>

// Needed with newest version of boost
// Otherwise the autolink in Visual C++ doesn't work
#ifdef _MSC_VER
#ifndef BOOST_TEST_MAIN
#define BOOST_TEST_MAIN
#endif
#endif

#include <boost/test/unit_test.hpp>

namespace esystest
{

namespace boost_impl
{

TestCaseCtrl *TestCaseCtrl::g_test_case = nullptr;

TestCaseCtrl &TestCaseCtrl::Get()
{
    assert(g_test_case != nullptr);

    return *g_test_case;
}

TestCaseCtrl::TestCaseCtrl()
    : TestCaseCtrlCore()
{
    g_test_case = this;

    set_strict_parsing(false);
    set_use_esystest(false);
}

TestCaseCtrl::~TestCaseCtrl() = default;

void TestCaseCtrl::setup()
{
    int result = 0;

    result = Init();
    if (result < 0)
    {
        BOOST_TEST_MESSAGE("TestCaseCtrl initialization failed!");
    }
}

void TestCaseCtrl::AddDefaultOptions()
{
    // clang-format off
    m_desc.add_options()
        ("help-all", "produce help message")
        ("list", "list all unit tests")
#ifdef ESYS_USE_VLD
        ("vld-off", "turn off vld")
#endif
        ("log_trace", po::value<std::string>(&m_log_trace_path)->implicit_value(""), "log calling traces")
        ("verbose,v", po::value<int>()->default_value(0), "set verbosity level: 0 is off")
        ("test_file_path", po::value<std::string>(&m_test_file_path_s), "set the path for test files")
        ("temp_file_path", po::value<std::string>(&m_temp_file_path_s), "set the path for temp files");
    // clang-format on
}

int TestCaseCtrl::GetArgC()
{
    return boost::unit_test::framework::master_test_suite().argc;
}

char **TestCaseCtrl::GetArgV()
{
    return boost::unit_test::framework::master_test_suite().argv;
}

} // namespace boost_impl

} // namespace esystest

/*!
 * \file esystest/esystest/unit_test_esystest.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esystest/esystest_prec.h"
#include "esystest/esystest/unit_test.h"

namespace esystest
{

ESYSTEST_API void nop()
{
}

} // namespace esystest

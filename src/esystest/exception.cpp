/*!
 * \file esystest/exception.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esystest/esystest_prec.h"
#include "esystest/exception.h"

namespace esystest
{

Exception::Exception(ID id)
    : m_id(id)
{
}

void Exception::SetId(ID id)
{
    m_id = id;
}

Exception::ID Exception::GetId() const
{
    return m_id;
}

} // namespace esystest

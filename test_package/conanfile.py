import os
from conan import ConanFile
from conan.tools.cmake import (
    CMakeToolchain,
    CMake,
    CMakeDeps,
)


class ESysTestTestConan(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    requires = "esystest/0.1", "boost/1.71.0"
    default_options = {
        "boost/*:shared": True,
        "libbacktrace/*:shared": True,
    }

    def generate(self):
        deps = CMakeDeps(self)
        deps.generate()
        tc = CMakeToolchain(self)
        tc.cache_variables["ESYSTEST_BUILD_VHW"] = 1
        tc.cache_variables["ESYSTEST_MULTIOS"] = 1
        tc.cache_variables["ESYSTEST_USE_BOOST"] = 1
        tc.cache_variables["ESYSTEST_USE_CONAN"] = True
        tc.user_presets_path = "ConanPresets.json"
        tc.generate()

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def test(self):
        self.run(".%sexample" % os.sep)

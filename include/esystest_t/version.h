/*!
 * \file esystest/version.h
 * \brief Version info for esystest_t
 *
 * \cond
 *__legal_b__
 *
 * Copyright (c) 2015-2020 Michel Gillet
 *Distributed under the MIT License.
 *(See accompanying file LICENSE.txt or
 *copy at https://opensource.org/licenses/MIT)
 *
 *__legal_e__
 * \endcond
 *
 */

#pragma once

// Bump-up with each new version
#define ESYSTEST_T_MAJOR_VERSION 0
#define ESYSTEST_T_MINOR_VERSION 0
#define ESYSTEST_T_RELEASE_NUMBER 1
#define ESYSTEST_T_VERSION_STRING "esystest_t 0.0.1"

// Must be updated manually as well each time the version above changes
#define ESYSTEST_T_VERSION_NUM_DOT_STRING "0.0.1"
#define ESYSTEST_T_VERSION_NUM_STRING "0001"

// nothing should be updated below this line when updating the version

#define ESYSTEST_T_VERSION_NUMBER \
    (ESYSTEST_T_MAJOR_VERSION * 1000) + (ESYSTEST_T_MINOR_VERSION * 100) + ESYSTEST_T_RELEASE_NUMBER
#define ESYSTEST_T_BETA_NUMBER 1
#define ESYSTEST_T_VERSION_FLOAT                                                                       \
    ESYSTEST_T_MAJOR_VERSION + (ESYSTEST_T_MINOR_VERSION / 10.0) + (ESYSTEST_T_RELEASE_NUMBER / 100.0) \
        + (ESYSTEST_T_BETA_NUMBER / 10000.0)

// check if the current version is at least major.minor.release
#define ESYSTEST_T_CHECK_VERSION(major, minor, release)                                                                \
    (ESYSTEST_T_MAJOR_VERSION > (major) || (ESYSTEST_T_MAJOR_VERSION == (major) && ESYSTEST_T_MINOR_VERSION > (minor)) \
     || (ESYSTEST_T_MAJOR_VERSION == (major) && ESYSTEST_T_MINOR_VERSION == (minor)                                    \
         && ESYSTEST_T_RELEASE_NUMBER >= (release)))

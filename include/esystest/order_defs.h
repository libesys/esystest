/*!
 * \file esystest/order_defs.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

namespace esystest
{

const unsigned int ORDER_NOT_SET = 0xFFFE;
const unsigned int ORDER_LAST = 0xFFFF;

} // namespace esystest

/*!
 * \file esystest/order.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esystest/esystest_defs.h"
#include "esystest/order_defs.h"

#ifdef ESYSTEST_USE_BOOST
#include "esystest/boost/order.h"
#else
#include "esystest/esystest/order.h"
#endif

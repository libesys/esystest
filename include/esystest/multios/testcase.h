/*!
 * \file esystest/multios/testcase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esystest/esystest_defs.h"
#include "esystest/testcasebase.h"

#include <string>

namespace esystest::multios
{

class ESYSTEST_API TestCase : public TestCaseBase
{
public:
    TestCase(TestCaseInfo *info = nullptr);
    ~TestCase() override;

    std::string create_test_temp_folder(const std::string &folder = "", bool delete_if_exist = true,
                                        bool test_name_lower_case = true);
};

} // namespace esystest::multios

#ifdef ESYSTEST_MULTIOS
namespace esystest
{
using namespace multios;
}
#endif

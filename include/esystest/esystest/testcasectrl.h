/*!
 * \file esystest/esystest/testcasectrl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esystest/esystest_defs.h"
#include "esystest/testcasectrlcore.h"

namespace esystest
{

namespace esystest
{

class ESYSTEST_API TestCaseCtrl : public TestCaseCtrlCore
{
public:
    TestCaseCtrl();
    virtual ~TestCaseCtrl();

    static TestCaseCtrl &Get();

protected:
    static TestCaseCtrl *g_test_case;
};

} // namespace esystest

#ifdef ESYSTEST_USE_ESYSTEST
using namespace esystest;
#endif

} // namespace esystest

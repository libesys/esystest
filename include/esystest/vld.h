/*!
 * \file esystest/vld.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2019-2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#if defined(WIN32) && defined(ESYS_USE_VLD)
#include <vld.h>
#endif

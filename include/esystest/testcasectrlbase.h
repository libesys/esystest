/*!
 * \file esystest/testcasectrlbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esystest/esystest_defs.h"
#ifdef ESYSTEST_USE_ESYS
#include <esys/inttypes.h>
#endif

namespace esystest
{

class ESYSTEST_API TestCaseInfo;

class ESYSTEST_API TestCaseCtrlBase
{
public:
    static const int NAME_SIZE = 50;

    TestCaseCtrlBase();
    virtual ~TestCaseCtrlBase();

    virtual int Init() = 0;

    virtual void BeforeTest() = 0;
    virtual void AfterTest() = 0;
    virtual void Invoke(TestCaseInfo *cur_test) = 0;
    virtual void Assert() = 0;

    void RunOneTest(char *name);
    bool GetRunAll();
    char *GetTestToRun();

    void PrintList();

    static TestCaseCtrlBase *GetBase();
    static void SetBase(TestCaseCtrlBase *);

protected:
    static TestCaseCtrlBase *s_ctrl;

    bool m_run_all;
    char m_test_case_name[NAME_SIZE + 1];
};

} // namespace esystest

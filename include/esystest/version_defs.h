/*!
 * \file esystest/version_defs.h
 * \brief Version definitions
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2021 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#define ESYSTEST_MAJOR_VERSION 0
#define ESYSTEST_MINOR_VERSION 1
#define ESYSTEST_RELEASE_NUMBER 0
#define ESYSTEST_PATCH_VERSION 0

#define ESYSTEST_BETA_NUMBER 0

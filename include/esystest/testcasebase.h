/*!
 * \file esystest/testcasebase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esystest/esystest_defs.h"

namespace esystest
{

class ESYSTEST_API TestCaseInfo;

class ESYSTEST_API TestCaseBase
{
public:
    TestCaseBase(TestCaseInfo *info = nullptr);
    virtual ~TestCaseBase();

    void set_info(TestCaseInfo *info);
    const TestCaseInfo *get_info() const;
    TestCaseInfo *get_info();

    virtual void TestMethod() = 0;

    static unsigned int GetCount();

protected:
    static unsigned int g_count;

    TestCaseInfo *m_info = nullptr;
};

} // namespace esystest

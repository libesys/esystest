/*!
 * \file esystest/testcasectrl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esystest/setup.h"

#if defined(ESYSTEST_USE_BOOST)
#include "esystest/boost/boost/testcasectrl.h"
#else
// By default use the command line Test Case Controller, which is not meant for embedded SW
#include "esystest/esystest/testcasectrl.h"
#endif

/*!
 * \file esystest/boost/testcasectrl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esystest/esystest_defs.h"
#include "esystest/testcasectrlcore.h"

namespace esystest
{

namespace boost_impl
{

class ESYSTEST_API TestCaseCtrl : public TestCaseCtrlCore
{
public:
    TestCaseCtrl();
    virtual ~TestCaseCtrl();

    void setup();

    virtual void AddDefaultOptions() override;
    virtual int GetArgC() override;
    virtual char **GetArgV() override;

    static TestCaseCtrl &Get();

protected:
    static TestCaseCtrl *g_test_case;
};

} // namespace boost_impl

#if defined(ESYSTEST_USE_BOOST)
using namespace boost_impl;
#endif

} // namespace esystest

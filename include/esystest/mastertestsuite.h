/*!
 * \file esystest/mastertestsuite.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esystest/esystest_defs.h"
#include "esystest/setup.h"
#include "esystest/testsuite.h"

#ifdef ESYSTEST_USE_ESYS
#include <esys/mutex.h>
#include <esys/module.h>
#endif

namespace esystest
{

class ESYSTEST_API MasterTestSuite : public TestSuite
#ifdef ESYSTEST_USE_ESYS
    ,
                                     public esys::Module
#endif
{
public:
#ifdef ESYSTEST_USE_ESYS
    MasterTestSuite(const esys::ObjectName &name = "");
#else
    MasterTestSuite(const char *name = nullptr);
#endif
    virtual ~MasterTestSuite();

#ifdef ESYSTEST_USE_ESYS
    const char *GetName();
    esys::Mutex &GetMutex();
#endif
    static MasterTestSuite &Get();

protected:
    static MasterTestSuite *s_master_test_suite;

#ifdef ESYSTEST_USE_ESYS
    esys::Mutex m_mutex;
#endif
};

} // namespace esystest

/*!
 * \file esystest/em/testcasectrlcore.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esystest/esystest_defs.h"
#include "esystest/testcasectrlbase.h"

namespace esystest
{

namespace em
{

class ESYSTEST_API TestCaseCtrlCore : public TestCaseCtrlBase
{
public:
    TestCaseCtrlCore();
    virtual ~TestCaseCtrlCore();

    virtual void Invoke(TestCaseInfo *cur_test) override;

protected:
    int m_verbose;
    bool m_log_trace;
};

} // namespace em

#ifdef ESYSTEST_EM
using namespace em;
#endif

} // namespace esystest

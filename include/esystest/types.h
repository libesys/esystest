/*!
 * \file esystest/types.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esystest/esystest_defs.h"

namespace esystest
{

class ESYSTEST_API TestCaseInfo;

struct nil_t
{
};

} // namespace esystest

ESYSTEST_API esystest::TestCaseInfo &operator*(esystest::TestCaseInfo &info, const esystest::nil_t &n);

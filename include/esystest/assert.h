/*!
 * \file esystest/assert.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#ifdef ESYSTEST_USE_ESYS
#include <esys/assert.h>
#else

#ifdef _MSC_VER
#include <assert.h>
#else
#include <cassert>
#endif

#endif

#ifndef assert
// #define assert
#endif

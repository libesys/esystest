from conan import ConanFile
from conan.tools.cmake import CMakeToolchain, CMake, CMakeDeps


class ESysTestConan(ConanFile):
    name = "esystest"
    version = "0.1.0"

    license = "wxWindows Library Licence, Version 3.1"
    author = "Michel Gillet michel.gillet@libesys.org"
    url = "https://gitlab.com/libesys/esystest"
    description = "C++ Unit Test Framework for both CPU and MCU"
    topics = ("c++", "testing", "UT", "unit testing")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    requires = "boost/1.71.0"
    default_options = {
        "shared": True,
        "fPIC": True,
        "boost/*:shared": True,
        "libbacktrace/*:shared": True,
    }
    exports_sources = [
        "include/*",
        "project/*",
        "src/*",
        "res/*",
        "samples/*",
        "scripts/*",
        "test_packege/*",
        "CMakeLists.txt",
    ]

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def generate(self):
        deps = CMakeDeps(self)
        deps.generate()
        tc = CMakeToolchain(self)
        tc.cache_variables["ESYSTEST_BUILD_VHW"] = 1
        tc.cache_variables["ESYSTEST_MULTIOS"] = 1
        tc.cache_variables["ESYSTEST_USE_BOOST"] = 1
        tc.cache_variables["ESYSTEST_USE_CONAN"] = True
        tc.user_presets_path = "ConanPresets.json"
        tc.generate()

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()
        cmake.install()

    def package(self):
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["esystest"]

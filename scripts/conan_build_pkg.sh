#!/bin/bash

echo "${TXT_S}Build Conan pkg ...${TXT_CLEAR}"
echo "pwd = "`pwd`

export CONAN_USER_HOME="$PWD/_conan"
echo "CONAN_USER_HOME=$CONAN_USER_HOME"

cd build/conan
pwd

conan install ../../conanfile.py -s:b compiler.cppstd=17 --build missing
if [ ! $? -eq 0 ]; then
   echo "${TXT_E}Couldn't install conan pacakged.${TXT_CLEAR}"
   exit 1
fi

echo "${TXT_S}conan create ../../conanfile.py -s: compiler.cppstd=17 ${TXT_CLEAR}"
conan create ../../conanfile.py -s:b compiler.cppstd=17
if [ ! $? -eq 0 ]; then
   echo "${TXT_E}Failed to create the package.${TXT_CLEAR}"
   exit 1
fi

echo "pwd = "`pwd`
echo "${TXT_S}Build Conan pkg done.${TXT_CLEAR}"

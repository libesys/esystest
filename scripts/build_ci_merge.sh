#!/bin/bash

echo "${TXT_S}Build ...${TXT_CLEAR}"
echo "pwd = "`pwd`

mkdir -p build/cmake
cd build/cmake
pwd

cmake ../.. -DESYSTEST_BUILD_DOC=On -DESYSTEST_COVERAGE=On -DCMAKE_BUILD_TYPE=Debug -DESYSTEST_UNIT_TESTS=On -DCMAKE_EXPORT_COMPILE_COMMANDS=ON
if [ ! $? -eq 0 ]; then
   echo "${TXT_E}Build failed: cmake failed.${TXT_CLEAR}"
   exit 1
fi

if [ "$(uname)" == "Darwin" ]; then
    N=`sysctl -n hw.logicalcpu`
else
    N=`nproc --all`
fi

echo "    ${TXT_S}Build esystest_t ...${TXT_CLEAR}"
make esystest_t -j$N
if [ ! $? -eq 0 ]; then
   echo "${TXT_E}Cmake failed.${TXT_CLEAR}"
   exit 1
fi
echo "    ${TXT_S}Build esystest_t done.${TXT_CLEAR}"

if [ -z "${SKIP_DOC}" ]; then
echo "    ${TXT_S}Build esystest_doc ...${TXT_CLEAR}"
make esystest_doc -j$N
if [ ! $? -eq 0 ]; then
   echo "${TXT_E}Build esystest_doc failed.${TXT_CLEAR}"
   exit 1
fi
echo "    ${TXT_S}Build esystest_doc done.${TXT_CLEAR}"
fi

mkdir -p ../../public/cpp_api
cp -R doc/html ../../public/cpp_api

echo "${TXT_S}Build done.${TXT_CLEAR}"

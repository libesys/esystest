#!/bin/bash

echo "${TXT_S}Run cland-tidy ...${TXT_CLEAR}"

cd build
cd cmake

mkdir -p ../../public/clang_tidy

make esystest-tidy-html -j`nproc --all`
RESULT_ESYSTEST_TIDY=$?

cp -R src/esystest/esystest-clang_tidy.html ../../public/clang_tidy
cp -R src/esystest/esystest-clang_tidy.log ../../public/clang_tidy

if [ ! ${RESULT_ESYSTEST_TIDY} -eq 0 ]; then
   echo "${TXT_W}Clang-tidy for eystest failed.${TXT_CLEAR}"
fi

echo "${TXT_S}Run cland-tidy done.${TXT_CLEAR}"

#!/bin/bash

echo "${TXT_S}Run unit tests ...${TXT_CLEAR}"

cd build/cmake

./src/esystest_t/esystest_t --logger=HRF,test_suite,stdout:JUNIT,all,esysos_t_report_junit.txt
ESYSTEST_T_RESULT_UT=$?

mkdir -p public/logs
cp *_junit.txt public/logs

UT_ERROR=0

if [ ! ${ESYSTEST_T_RESULT_UT} -eq 0 ]; then
   echo "${TXT_E}Unit tests esystest_t failed.${TXT_CLEAR}"
   UT_ERROR=1
fi

if [ ! ${UT_ERROR} -eq 0 ]; then
   exit 1
fi

echo "${TXT_S}Run unit tests done.${TXT_CLEAR}"

#!/bin/bash

echo "${TXT_S}Run SonarCloud scanner ...${TXT_CLEAR}"

if [ -z "${SKIP_SONAR_CLOUD}" ]; then
    if [ -z "${SKIP_CODE_COVERAGE}" ]; then
        sonar-scanner/bin/sonar-scanner \
            -Dsonar.host.url="${SONAR_HOST_URL}" \
            -Dsonar.token="${SONAR_TOKEN}" \
            -Dsonar.cfamily.compile-commands=build/cmake/compile_commands.json \
            -Dsonar.coverageReportPaths=build/cmake/esystest_coverage_sonarqube.xml \
            -X
    else
        sonar-scanner/bin/sonar-scanner \
            -Dsonar.host.url="${SONAR_HOST_URL}" \
            -Dsonar.token="${SONAR_TOKEN}" \
            -Dsonar.cfamily.compile-commands=build/cmake/compile_commands.json \
            -X
    fi
fi

echo "${TXT_S}Run SonarCloud scanner done.${TXT_CLEAR}"

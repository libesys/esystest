#!/bin/bash

echo "${TXT_S}Run code coverage on unit tests ...${TXT_CLEAR}"

cd build
cd cmake

if [ "$(uname)" == "Darwin" ]; then
   N=`sysctl -n hw.logicalcpu`
else
   N=`nproc --all`
fi

mkdir -p ../../public/coverage
mkdir -p ../../public/logs

make esystest_coverage -j$N
RESULT_ESYSTEST_T_UT=$?

lcov --list esystest_coverage.info

cp -R esystest_coverage/* ../../public/coverage
cp esystest_coverage.info ../../public/logs/esystest_coverage.info
cp report.junit ../../public/logs/coverage_esystest_junit.txt

COVERAGE_ERROR=0

if [ ! ${RESULT_ESYSTEST_T_UT} -eq 0 ]; then
   echo "${TXT_E}Code coverage on unit tests esystest_t failed.${TXT_CLEAR}"
   COVERAGE_ERROR=1
fi

if [ ! ${COVERAGE_ERROR} -eq 0 ]; then
   exit 1
fi

echo "${TXT_S}Run code coverage on unit tests done.${TXT_CLEAR}"

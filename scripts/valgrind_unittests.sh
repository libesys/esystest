#!/bin/bash

echo "${TXT_S}Valgrind unit test starts ...${TXT_CLEAR}"
echo "pwd = "`pwd`

mkdir -p build/cmake
cd build/cmake

mkdir -p ../../public/logs

mkdir -p ../../public/esystest_t/valgrind

make esystest_t-valgrind-html -j`nproc --all`

cp src/esystest_t/*_junit.txt ../../../public/logs
cp src/esystest_t/esystest_t-valgrind-out.* ../../public/logs
cp src/esystest_t/esystest_t-valgrind-out.* ../../public/esystest_t/valgrind
cp -R src/esystest_t/valgrind_html/* ../../public/esystest_t/valgrind

mkdir -p ../../public/esystestboost_t/valgrind

make esystestboost_t-valgrind-html -j`nproc --all`

cp src/esystest_t/*_junit.txt ../../public/logs
cp src/esystest_t/esystestboost_t-valgrind-out.* ../../public/logs
cp src/esystest_t/esystestboost_t-valgrind-out.* ../../public/esystestboost_t/valgrind
cp -R src/esystest_t/valgrind_html/* ../../public/esystestboost_t/valgrind

echo "pwd = "`pwd`
echo "${TXT_S}Valgrind unit test done.${TXT_CLEAR}"

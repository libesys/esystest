#!/bin/bash

echo "${TXT_S}ESysTest Boostrap ...${TXT_CLEAR}"

git clone https://gitlab.com/libesys/esys_hugo_theme.git src/esys_hugo_theme
git clone https://gitlab.com/libesys/esys_cmake_scripts src/esys_cmake_scripts
git clone -b create_sonarqube_output_and_lcov https://github.com/libESys/cmake-modules.git extlib/cmake-modules

echo "${TXT_S}ESysTest Boostrap Done.${TXT_CLEAR}"

---
title: Code Coverage
weight: 20
pre: "4.2 "
---

The code coverage results can be found [here](https://libesys.gitlab.io/esystest/coverage/) or directly seen below.

{{< unsafe >}}
<iframe src="https://libesys.gitlab.io/esystest/coverage" width="100%" height="800px"></iframe>
{{< /unsafe >}}

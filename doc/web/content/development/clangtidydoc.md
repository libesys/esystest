---
title: Clang-tidy
weight: 30
pre: "4.3. "
---

The results of running clang-tidy are [here](https://libesys.gitlab.io/esystest/clang_tidy/esysio-clang_tidy.html) or directly seen below.

{{< unsafe >}}
<iframe src="https://libesys.gitlab.io/esystest/clang_tidy/esysio-clang_tidy.html" width="100%" height="800px"></iframe>
{{< /unsafe >}}
